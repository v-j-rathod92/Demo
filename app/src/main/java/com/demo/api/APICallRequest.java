package com.demo.api;

import java.util.HashMap;

/**
 * Created by Vishal Rathod on 19/8/17.
 */

public class APICallRequest {
    private String baseUrl;
    private String endPoint;
    private Method method;
    private HashMap<String, String> params;
    private HashMap<String, String> headers;

    private APICallRequest(APICallRequestBuilder apiCallRequestBuilder) {
        this.baseUrl =
        this.endPoint = apiCallRequestBuilder.endPoint;
        this.method = apiCallRequestBuilder.method;
        this.params = apiCallRequestBuilder.params;
        this.headers = apiCallRequestBuilder.headers;
    }

    public static class APICallRequestBuilder {
        private String baseUrl;
        private String endPoint;
        private Method method;
        private HashMap<String, String> params;
        private HashMap<String, String> headers;

        public APICallRequestBuilder baseUrl(String baseUrl){
            this.baseUrl = baseUrl;
            return this;
        }

        public APICallRequestBuilder method(Method method) {
            this.method = method;
            return this;
        }

        public APICallRequestBuilder endPoint(String endPoint) {
            this.endPoint = endPoint;
            return this;
        }

        public APICallRequestBuilder params(HashMap<String, String> params) {
            this.params = params;
            return this;
        }

        public APICallRequestBuilder headers(HashMap<String, String> headers){
            this.headers = headers;
            return this;
        }

        public APICallRequest fireRequest() {
            return new APICallRequest(this);
        }
    }

    enum Method {
        GET, POST, PUT, PATCH, DELETE;
    }
}
