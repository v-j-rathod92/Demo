package com.demo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Vishal Rathod on 23/8/17.
 */

public class CheckView extends View {
    private Paint paint;
    private Path path;

    public CheckView(Context context) {
        super(context);
        init();
    }

    public CheckView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.RED);
        path = new Path();
        path.lineTo(0, 10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawPath(path, paint);
    }
}
