package com.demo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.demo.util.ScreenUtil;

/**
 * Created by Vishal Rathod on 2/8/17.
 */

public class DemoActivity extends AppCompatActivity {

    private EditText edtSearch;
    private Button btnPlay, btnSearch, btnSearchComplete;
    private Context mContext;
    private RelativeLayout relative;
    private ImageView imgView;
    private ValueAnimator animSearching;
    private Integer x;
    private Handler handler = new Handler();
    private View view;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_test);

        mContext = this;

        edtSearch = (EditText) findViewById(R.id.edtSearch);
        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearchComplete = (Button) findViewById(R.id.btnSearchComplete);
        relative = (RelativeLayout) findViewById(R.id.relative);
        imgView = (ImageView) findViewById(R.id.imgView);
        view = (View) findViewById(R.id.view);

        relative.setX(ScreenUtil.getScreenWidth() / 2);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                // move center to left of screen
                final ValueAnimator valueAnimator = ValueAnimator.ofFloat(relative.getX(), ScreenUtil.dpToPx(10));
                valueAnimator.setDuration(300);
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Float position = (Float) animation.getAnimatedValue();
                        relative.setX(position);
                    }

                });

                // make with match parent
                ValueAnimator widthAnimation = ValueAnimator.ofInt(ScreenUtil.dpToPx(50),
                        ScreenUtil.getScreenWidth() - ScreenUtil.dpToPx(10) - ScreenUtil.dpToPx(10));
                widthAnimation.setDuration(300);
                widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {

                        Integer width = (Integer) animation.getAnimatedValue();
                        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(width, ScreenUtil.dpToPx(50));
                        param.addRule(RelativeLayout.CENTER_VERTICAL);
                        param.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

                        relative.setLayoutParams(param);

                    }
                });


                // choreograph animation
                AnimatorSet anim = new AnimatorSet();
                anim.play(valueAnimator).before(widthAnimation);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        // zoom in out animation
                        imgView.animate().scaleX(0).scaleY(0).setDuration(300).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                imgView.setImageResource(android.R.drawable.ic_search_category_default);
                                imgView.animate().scaleX(1).scaleY(1).setDuration(300).start();
                            }
                        }).start();
                    }
                });
                anim.start();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int left = edtSearch.getLeft();
                int right = edtSearch.getRight();
                final int middle = (left + right) / 2;

                edtSearch.setEnabled(false);

                // shrink search bar animation
                ValueAnimator shrinkAnim = ValueAnimator.ofInt(edtSearch.getHeight(), 2);
                shrinkAnim.setDuration(300);
                shrinkAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Integer integer = (Integer) animation.getAnimatedValue();

                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) edtSearch.getLayoutParams();
                        layoutParams.height = ScreenUtil.dpToPx(integer);
                        edtSearch.setLayoutParams(layoutParams);
                    }
                });

                shrinkAnim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        // animate search icon while progress
                        animSearching = ValueAnimator.ofInt((int) imgView.getX(), left);
                        animSearching.setRepeatCount(ValueAnimator.INFINITE);
                        animSearching.setRepeatMode(ValueAnimator.REVERSE);
                        animSearching.setDuration(1000);
                        animSearching.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator animation) {
                                x = (Integer) animation.getAnimatedValue();
                                imgView.setX(x);
                            }
                        });


                        animSearching.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(final Animator animation) {
                                super.onAnimationEnd(animation);

                                // stop searching to middle of screen with animation
                                imgView.animate().x(middle - ScreenUtil.dpToPx(25)).setDuration(1000).withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        // animate check mark animation
                                        imgView.setImageResource(R.drawable.animated_star);
                                        Drawable drawable = imgView.getDrawable();
                                        if (drawable instanceof Animatable) {
                                            ((Animatable) drawable).start();

                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {

                                                    imgView.setImageResource(android.R.drawable.ic_search_category_default);
                                                    ValueAnimator expandAnim = ValueAnimator.ofInt(edtSearch.getHeight(), ScreenUtil.dpToPx(50));
                                                    expandAnim.setDuration(300);
                                                    expandAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                        @Override
                                                        public void onAnimationUpdate(ValueAnimator animation) {
                                                            Integer integer = (Integer) animation.getAnimatedValue();

                                                            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) edtSearch.getLayoutParams();
                                                            layoutParams.height = ScreenUtil.dpToPx(integer);
                                                            edtSearch.setLayoutParams(layoutParams);
                                                        }
                                                    });

                                                    ValueAnimator widthAnim = ValueAnimator.ofInt(edtSearch.getWidth(), ScreenUtil.dpToPx(50));
                                                    widthAnim.setDuration(200);
                                                    widthAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                        @Override
                                                        public void onAnimationUpdate(ValueAnimator animation) {
                                                            Integer integer = (Integer) animation.getAnimatedValue();

                                                            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relative.getLayoutParams();
                                                            layoutParams.width = integer;
                                                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                                                            relative.setLayoutParams(layoutParams);

                                                            int w = relative.getWidth();
                                                            int middle = (w / 2) - (imgView.getWidth() / 2);
                                                            imgView.setX(middle);

                                                        }
                                                    });

                                                    AnimatorSet animatorSet = new AnimatorSet();
                                                    animatorSet.play(expandAnim).with(widthAnim);
                                                    animatorSet.addListener(new AnimatorListenerAdapter() {
                                                        @Override
                                                        public void onAnimationEnd(Animator animation) {
                                                            super.onAnimationEnd(animation);

                                                            relative.animate().x(view.getX()).y(view.getY()).setDuration(500).start();
                                                        }
                                                    });
                                                    animatorSet.start();

                                                }
                                            }, 1000);
                                        }
                                    }
                                }).start();
                            }
                        });
                        animSearching.start();
                    }
                });

                shrinkAnim.start();
            }
        });

        btnSearchComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // cancel searching
                animSearching.cancel();
            }
        });
    }

    public void sleepRoom(View view) {
        final RelativeLayout sr = (RelativeLayout) view;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) sr.getLayoutParams();
        params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        sr.setLayoutParams(params);

        PropertyValuesHolder pvhLeft = PropertyValuesHolder.ofInt("left", 0, 1);
        PropertyValuesHolder pvhTop = PropertyValuesHolder.ofInt("top", 0, 1);
        PropertyValuesHolder pvhRight = PropertyValuesHolder.ofInt("right", 0, 1);
        PropertyValuesHolder pvhBottom = PropertyValuesHolder.ofInt("bottom", 0, 1);
        PropertyValuesHolder pvhRoundness = PropertyValuesHolder.ofFloat("roundness", 0, 1);


        final Animator collapseExpandAnim = ObjectAnimator.ofPropertyValuesHolder(sr, pvhLeft, pvhTop,
                pvhRight, pvhBottom, pvhRoundness);
        collapseExpandAnim.setupStartValues();

        sr.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                sr.getViewTreeObserver().removeOnPreDrawListener(this);
                collapseExpandAnim.setupEndValues();
                collapseExpandAnim.start();
                return false;
            }
        });
    }
}
