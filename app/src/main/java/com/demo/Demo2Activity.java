package com.demo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

/**
 * Created by Vishal Rathod on 18/8/17.
 */

public class Demo2Activity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtEmail, edtPassword, edtMobile;
    private Button btnSignIn, btnRegister, btnReset, btnMobile, btnVerify;
    private FirebaseAuth auth;
    private String verificationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        init();
        setListeners();

    }

    private void init() {
        auth = FirebaseAuth.getInstance();

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtMobile = (EditText) findViewById(R.id.edtMobile);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnMobile = (Button) findViewById(R.id.btnMobile);
        btnVerify = (Button) findViewById(R.id.btnVerify);
    }

    private void setListeners() {
        btnSignIn.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnMobile.setOnClickListener(this);
        btnVerify.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignIn:
                performAction(1);
                break;

            case R.id.btnRegister:
                performAction(2);
                break;

            case R.id.btnReset:
                performAction(3);
                break;

            case R.id.btnMobile:
                String mobile = edtMobile.getText().toString().trim();

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        mobile,
                        1,
                        TimeUnit.MINUTES,
                        this,
                        new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                            @Override
                            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                                Log.e("phone verification complete", "ok");
                            }

                            @Override
                            public void onVerificationFailed(FirebaseException e) {
                                Log.e("phone verfication failed", e.toString());
                            }

                            @Override
                            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                Log.e("code sent", "ok");
                                verificationId = s;
                            }
                        });
                break;


            case R.id.btnVerify:
                PhoneAuthCredential pac = PhoneAuthProvider.getCredential(verificationId, edtMobile.getText().toString());
                auth.signInWithCredential(pac)
                        .addOnCompleteListener(Demo2Activity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Log.e("mobile sign in success", "ok");
                                } else {
                                    Log.e("mobile sign in failed", task.getException().toString());
                                }
                            }
                        });
                break;
        }
    }

    private void performAction(int action) {
        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if (action == 1) {
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.e("sign in success", "ok");
                            } else {
                                Log.e("sign in failed", task.getException().toString());
                            }
                        }
                    });
        } else if (action == 2) {
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                Log.e("register success", "ok");
                                FirebaseUser user = auth.getCurrentUser();
                                if (user == null) {
                                    user = task.getResult().getUser();
                                }

                                if (user != null) {
                                    user.sendEmailVerification()
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.e("email verificaiton", "ok");
                                                    } else {
                                                        Log.e("email verificaiton failed", task.getException().toString());
                                                    }
                                                }
                                            });
                                }
                            } else {
                                Log.e("register failed", task.getException().toString());
                            }
                        }
                    });
        } else {
            auth.sendPasswordResetEmail(email)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.e("password sent", "ok");
                            } else {
                                Log.e("password failed", task.getException().getMessage());
                            }
                        }
                    });
        }
    }
}
