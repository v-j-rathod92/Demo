package com.demo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Vishal Rathod on 9/8/17.
 */

public class SwagPoint extends View {
    private int mProgressWidth = 12;
    private int mArcWidth = 12;
    private float mTextSize = 72;
    private Drawable mIndicatorIcon;
    public static final int MAX = 100;
    public static final int MIN = 0;
    private int mPoints = MIN;
    private int mMin = MIN;
    private int mMax = MAX;
    private int mStep = 10;
    private boolean mClockwise = true;
    private boolean mEnabled = true;
    private int mTextColor = Color.BLACK;
    private int mTranslateX, mTranslateY;

    public SwagPoint(Context context) {
        super(context);
        init(context, null);
    }

    public SwagPoint(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        float density = getResources().getDisplayMetrics().density;

        int arcColor = ContextCompat.getColor(context, R.color.color_arc);
        int progressColor = ContextCompat.getColor(context, R.color.color_progress);
        int textColor = ContextCompat.getColor(context, R.color.color_text);

        mProgressWidth = (int) (mProgressWidth * density);
        mArcWidth = (int) (mArcWidth * density);
        mTextSize = (int) (mTextSize * mTextSize);

        if(attrs != null){
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SwagPoint, 0, 0);

            Drawable indicatorIcon = a.getDrawable(R.styleable.SwagPoint_indicatorIcon);
            if(indicatorIcon != null){
                mIndicatorIcon = indicatorIcon;
            }

            int indicatorIconHalfWith = mIndicatorIcon.getIntrinsicWidth() / 2;
            int indicatorIconHalfHeight = mIndicatorIcon.getIntrinsicHeight() / 2;

            mIndicatorIcon.setBounds(-indicatorIconHalfWith, -indicatorIconHalfHeight, indicatorIconHalfWith, indicatorIconHalfHeight);

            mPoints = a.getInteger(R.styleable.SwagPoint_points, mPoints);
            mMin = a.getInteger(R.styleable.SwagPoint_min, mMin);
            mMax = a.getInteger(R.styleable.SwagPoint_max, mMax);
            mStep = a.getInteger(R.styleable.SwagPoint_step, mStep);


            mProgressWidth = (int) a.getDimension(R.styleable.SwagPoint_progressWidth, mProgressWidth);
            progressColor = a.getColor(R.styleable.SwagPoint_progressColor, progressColor);

            mArcWidth = (int) a.getDimension(R.styleable.SwagPoint_arcWidth, mArcWidth);
            arcColor = a.getColor(R.styleable.SwagPoint_arcColor, arcColor);

            mTextSize = (int) a.getDimension(R.styleable.SwagPoint_textSize, mTextSize);
            mTextColor = a.getColor(R.styleable.SwagPoint_textColor, mTextColor);

            mClockwise = a.getBoolean(R.styleable.SwagPoint_clockWise,
                    mClockwise);
            mEnabled = a.getBoolean(R.styleable.SwagPoint_enabled, mEnabled);

            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        mTranslateX = (int) (width * 0.5);
        mTranslateY = (int) (height * 0.5);
    }
}
